package com.si2001.officedoor.api

import com.si2001.officedoor.model.response.DoorResponse
import com.si2001.officedoor.model.response.OpenDoorResponse
import retrofit2.Call
import retrofit2.http.*

interface DoorApi {

    @FormUrlEncoded
    @POST("arduino-operations/open-door/{sede}")
    fun openDoor(
        @Path("sede") sede: String,
        @Header("Authorization") token: String,
        @Field("porta") porta: String
    ) : Call<OpenDoorResponse>

    @Headers(
        "Content-Type: application/json; charset=utf-8",
        "Accept: application/json"
    )
    @GET("porte/{sede}")
    fun getDoors(
        @Path("sede") sede: String,
        @Header("Authorization") token: String
    ) : Call<List<DoorResponse>>

}