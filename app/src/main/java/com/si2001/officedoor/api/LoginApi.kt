package com.si2001.officedoor.api

import com.si2001.officedoor.model.request.LoginRequest
import com.si2001.officedoor.model.response.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface LoginApi {

    @Headers(
        "Content-Type: application/json; charset=utf-8",
        "Accept: application/json"
    )
    @POST("authenticate")
    fun login(
        @Body loginRequest: LoginRequest
    ): Call<LoginResponse>


}