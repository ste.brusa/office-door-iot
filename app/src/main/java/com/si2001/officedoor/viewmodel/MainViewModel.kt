package com.si2001.officedoor.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.si2001.officedoor.model.request.DoorRequest
import com.si2001.officedoor.model.request.OpenDoorRequest
import com.si2001.officedoor.model.response.DoorResponse
import com.si2001.officedoor.model.response.OpenDoorResponse
import com.si2001.officedoor.repository.DoorRepositoryImpl
import javax.inject.Inject

class MainViewModel: ViewModel() {

    @Inject
    lateinit var doorRepositoryImpl: DoorRepositoryImpl

    val openDoorResponse = MutableLiveData<OpenDoorResponse>()

    val doors = MutableLiveData<List<DoorResponse>>()

    val openDoorError = MutableLiveData<String>()

    fun openDoor(openDoorRequest: OpenDoorRequest){
        doorRepositoryImpl.openDoor(openDoorRequest) {

            if(it?.sede != null){
                openDoorResponse.postValue(it)
            } else openDoorError.postValue(it?.error)

        }
    }

    fun getDoors(doorRequest: DoorRequest){
        doorRepositoryImpl.getDoors(doorRequest) {

            if(it != null){
                doors.postValue(it)
            }// else openDoorError.postValue(it?.error)

        }
    }

}
