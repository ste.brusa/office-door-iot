package com.si2001.officedoor.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.si2001.officedoor.model.response.LoginResponse
import com.si2001.officedoor.repository.LoginRepositoryImpl
import javax.inject.Inject

class LoginViewModel: ViewModel() {

    @Inject
    lateinit var loginRepository: LoginRepositoryImpl

    val username = MutableLiveData("")

    val password = MutableLiveData("")

    val usernameError = MutableLiveData<String>()

    val passwordError = MutableLiveData<String>()

    val loginResult = MutableLiveData<LoginResponse?>()

    fun logout(){

    }


    fun login(){
        username.value?.let { un ->
            if(android.util.Patterns.EMAIL_ADDRESS.matcher(un).matches()){
                usernameError.postValue("")

                password.value?.let { pwd ->
                    if(pwd.isNotEmpty()){
                        passwordError.postValue("")
                        loginRepository.login(un, pwd)
                        {
                                loginResponse: LoginResponse? -> loginResult.postValue(loginResponse)
                        }
                    } else passwordError.postValue("Password vuota")

                }
            } else {
                usernameError.postValue("Email non valida")
            }

        }

    }
}