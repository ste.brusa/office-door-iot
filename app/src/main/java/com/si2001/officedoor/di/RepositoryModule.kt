package com.si2001.officedoor.di

import com.si2001.officedoor.api.DoorApi
import com.si2001.officedoor.api.LoginApi
import com.si2001.officedoor.repository.DoorRepositoryImpl
import com.si2001.officedoor.repository.LoginRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideLoginRepository(loginApi: LoginApi) : LoginRepositoryImpl = LoginRepositoryImpl(loginApi)

    @Provides
    @Singleton
    fun provideDoorRepository(doorApi: DoorApi) : DoorRepositoryImpl = DoorRepositoryImpl(doorApi)

}