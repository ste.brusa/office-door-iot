package com.si2001.officedoor.di

class ComponentInjector {

    companion object {
        lateinit var component: AppComponent

        fun init() {
            component = DaggerAppComponent.builder().build()
        }

    }
}