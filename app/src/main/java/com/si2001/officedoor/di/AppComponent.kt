package com.si2001.officedoor.di

import com.si2001.officedoor.viewmodel.LoginViewModel
import com.si2001.officedoor.viewmodel.MainViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(
    modules = [ApiModule::class, RepositoryModule::class]
)
interface AppComponent {

    fun inject( viewModel: MainViewModel)
    fun inject( viewModel: LoginViewModel)
}