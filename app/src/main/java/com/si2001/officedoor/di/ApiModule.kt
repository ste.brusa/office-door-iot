package com.si2001.officedoor.di

import com.google.gson.Gson
import com.si2001.officedoor.api.DoorApi
import com.si2001.officedoor.api.LoginApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class ApiModule {

    private val client = OkHttpClient.Builder()
        .callTimeout(1, TimeUnit.MINUTES)
        .connectTimeout(10, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(10, TimeUnit.SECONDS)
        .build()

    @Provides
    @Singleton
    fun provideLoginApiService(): LoginApi {
        return Retrofit.Builder()
            .baseUrl("https://arduino-door.herokuapp.com/api/v1/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(client)
            .build().create(LoginApi::class.java)
    }

    @Provides
    @Singleton
    fun provideDoorApiService(): DoorApi {
        return Retrofit.Builder()
            .baseUrl("https://arduino-door.herokuapp.com/api/v1/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(client)
            .build().create(DoorApi::class.java)
    }


}