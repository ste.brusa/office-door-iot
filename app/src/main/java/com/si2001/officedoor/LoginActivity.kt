package com.si2001.officedoor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.si2001.officedoor.databinding.ActivityLoginBinding
import com.si2001.officedoor.di.ComponentInjector
import com.si2001.officedoor.utils.getSharedPreferenceToken
import com.si2001.officedoor.utils.isOnline
import com.si2001.officedoor.utils.setSharedPreferenceCompany
import com.si2001.officedoor.utils.setSharedPreferenceToken
import com.si2001.officedoor.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by lazy {
        ViewModelProvider(this).get(LoginViewModel::class.java).also {
            ComponentInjector.component.inject(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!getSharedPreferenceToken(this).isNullOrEmpty()) {
            startActivity(Intent(applicationContext, MainActivity::class.java).also {
                finish()
            })
        } else {

            supportActionBar?.hide()

            val binding: ActivityLoginBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_login)
            binding.viewmodel = viewModel
            binding.lifecycleOwner = this

            viewModel.passwordError.observe(this, Observer {
                passwordTextInputLayout.error = it
            })

            viewModel.usernameError.observe(this, Observer {
                emailTextInputLayout.error = it
            })

            viewModel.loginResult.observe(this, Observer {
                if (it?.token == null) {
                    Snackbar.make(loginButton, it?.error.toString(), Snackbar.LENGTH_LONG).show()
                } else {
                    it.token?.let { token ->
                        setSharedPreferenceToken(this, token)
                        it.azienda?.let { company ->
                            setSharedPreferenceCompany(this, company)
                        }
                        startActivity(Intent(applicationContext, MainActivity::class.java).also {
                            finish()
                        })
                    }
                }

                loading.visibility = View.GONE

            })

        loginButton.setOnClickListener {
            if(isOnline(this)) {
                viewModel.login()
                loading.visibility = View.VISIBLE
            }
            else Snackbar.make(loginButton, "Abilitare connessione ad internet", Snackbar.LENGTH_LONG).show()
        }

        }
    }

}
