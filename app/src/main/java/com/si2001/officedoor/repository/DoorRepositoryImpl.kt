package com.si2001.officedoor.repository

import android.util.Log
import com.si2001.officedoor.api.DoorApi
import com.si2001.officedoor.model.request.DoorRequest
import com.si2001.officedoor.model.request.OpenDoorRequest
import com.si2001.officedoor.model.response.DoorResponse
import com.si2001.officedoor.model.response.OpenDoorResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DoorRepositoryImpl( private val doorApi: DoorApi): DoorRepository {

    override fun openDoor( openDoorRequest: OpenDoorRequest, handler: (OpenDoorResponse?) -> Unit) {

        doorApi.openDoor( openDoorRequest.sede, openDoorRequest.token, openDoorRequest.doorType.toString()).enqueue(
            object : Callback<OpenDoorResponse>{
                override fun onFailure(call: Call<OpenDoorResponse>, t: Throwable) {
                    handler(OpenDoorResponse().also {
                        it.error = t.localizedMessage
                    })
                }

                override fun onResponse(
                    call: Call<OpenDoorResponse>,
                    response: Response<OpenDoorResponse>
                ) {
                    if(response.isSuccessful){
                        handler(response.body())
                    } else handler(OpenDoorResponse().also {
                        it.error = response.errorBody()?.string()
                    })

                }


            }
        )

    }

    override fun getDoors( doorRequest: DoorRequest, handler: (List<DoorResponse>?) -> Unit) {

        doorApi.getDoors( doorRequest.sede, doorRequest.token).enqueue(
            object : Callback<List<DoorResponse>>{
                override fun onFailure(call: Call<List<DoorResponse>>, t: Throwable) {
                    handler(null)
                }

                override fun onResponse(
                    call: Call<List<DoorResponse>>,
                    response: Response<List<DoorResponse>>
                ) {
                    if(response.isSuccessful){
                        handler(response.body())
                    } else handler(null)

                }
            }
        )

    }

}