package com.si2001.officedoor.repository

import com.si2001.officedoor.model.response.LoginResponse

interface LoginRepository {

    /**
     * Logga un utente se registrato e attivato
     * */
    fun login( username: String, password: String, handler : (LoginResponse?)-> Unit)


    /**
     * Effettua il logout dalla sessione corrente
     * */
    fun logout()
}