package com.si2001.officedoor.repository

import android.os.Handler
import android.util.Log
import com.si2001.officedoor.api.LoginApi
import com.si2001.officedoor.model.request.LoginRequest
import com.si2001.officedoor.model.response.LoginResponse
import com.si2001.officedoor.model.response.OpenDoorResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginRepositoryImpl(val loginApi: LoginApi): LoginRepository {

    override fun login(username: String, password: String, handler : (LoginResponse? )-> Unit) {
        loginApi.login( LoginRequest(username, password)).enqueue(
            object : Callback<LoginResponse>{

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    handler(LoginResponse().also {
                        it.error = t.localizedMessage
                    })
                }

                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {

                    if(response.isSuccessful){
                        handler( response.body())
                    } else {
                        handler(LoginResponse().also {
                            it.error = "Login errata"
                        })
                    }
                    Log.e("LOGIN RESPONSE", response.message())

                }

            }
        )
    }

    override fun logout() {

    }


}