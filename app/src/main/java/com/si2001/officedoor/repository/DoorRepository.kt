package com.si2001.officedoor.repository

import com.si2001.officedoor.model.request.DoorRequest
import com.si2001.officedoor.model.request.OpenDoorRequest
import com.si2001.officedoor.model.response.DoorResponse
import com.si2001.officedoor.model.response.OpenDoorResponse

interface DoorRepository {

    /**
     * Apre la porta se entro un certo raggio di Treviolo o Sestro San Giovanni
     * */
    fun openDoor(openDoorRequest: OpenDoorRequest, handler: (OpenDoorResponse?) -> Unit)

    fun getDoors(doorRequest: DoorRequest, handler: (List<DoorResponse>?) -> Unit)
}