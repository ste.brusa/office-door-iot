package com.si2001.officedoor

import android.app.Application
import com.si2001.officedoor.di.ComponentInjector

class OfficeDoorApplication: Application() {

    override fun onCreate() {

        // Dagger Component Injector
        ComponentInjector.init()
        super.onCreate()
    }
}