package com.si2001.officedoor.model.request

import com.google.gson.annotations.SerializedName

/**
 * Oggetto da passare come body alla richiesta di login
 * */

data class LoginRequest (

    @SerializedName("email")
    val email: String,

    @SerializedName("password")
    val password: String
)