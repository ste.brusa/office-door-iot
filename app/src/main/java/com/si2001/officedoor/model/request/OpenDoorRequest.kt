package com.si2001.officedoor.model.request

import com.google.gson.annotations.SerializedName
import com.si2001.officedoor.model.DoorType

/**
 * Oggetto i cui campi servono ad effetturare la chiamate per l'apertura della porta
 * */

data class OpenDoorRequest (
    @SerializedName("sede")
    val sede: String,

    @SerializedName("token")
    val token: String,

    @SerializedName("door_type")
    val doorType : DoorType
)