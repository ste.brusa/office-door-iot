package com.si2001.officedoor.model.request

import com.google.gson.annotations.SerializedName

data class DoorRequest (

    @SerializedName("sede")
    val sede: String,

    @SerializedName("token")
    val token: String
)