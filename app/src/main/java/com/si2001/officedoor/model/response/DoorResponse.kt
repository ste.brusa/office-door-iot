package com.si2001.officedoor.model.response

import com.google.gson.annotations.SerializedName

class DoorResponse {

    @SerializedName("porta")
    val porta: String? = null

    @SerializedName("url")
    val url: String? = null
}