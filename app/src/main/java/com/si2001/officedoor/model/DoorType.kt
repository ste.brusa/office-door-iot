package com.si2001.officedoor.model

import com.google.gson.annotations.SerializedName

enum class DoorType {
    @SerializedName("porta interna")
    IN,
    @SerializedName("porta esterna")
    OUT
}