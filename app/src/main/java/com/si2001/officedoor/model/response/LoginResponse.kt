package com.si2001.officedoor.model.response

import com.google.gson.annotations.SerializedName

class LoginResponse  {

    @SerializedName("token")
    var token: String? = null

    @SerializedName("azienda")
    var azienda: String? = null

    var code: Int = 0

    var error: String? = null


}