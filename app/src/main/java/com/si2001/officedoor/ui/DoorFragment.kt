package com.si2001.officedoor.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.location.LocationManagerCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.si2001.officedoor.LoginActivity
import com.si2001.officedoor.MainActivity
import com.si2001.officedoor.R
import com.si2001.officedoor.di.ComponentInjector
import com.si2001.officedoor.model.DoorType
import com.si2001.officedoor.model.request.DoorRequest
import com.si2001.officedoor.model.request.OpenDoorRequest
import com.si2001.officedoor.utils.*
import com.si2001.officedoor.viewmodel.LoginViewModel
import com.si2001.officedoor.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_door.*
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


class DoorFragment : Fragment(), LocationListener {

    private val requestPermissions = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { result ->
        // the result from RequestMultiplePermissions is a map linking each
        // request permission to a boolean of whether it is GRANTED

        // check if the permission is granted
        if (result[Manifest.permission.ACCESS_COARSE_LOCATION] == true && result[Manifest.permission.ACCESS_FINE_LOCATION] == true) {
            getCurrentLocation()
        } else {
            Snackbar.make(open_in_door_button, "Non hai concesso i permessi al GPS", Snackbar.LENGTH_LONG).show()
        }

    }

    private val mainViewModel by lazy { ViewModelProvider(this).get(MainViewModel::class.java).also {
        ComponentInjector.component.inject(it)
    } }

    private val loginViewModel by lazy { ViewModelProvider(this).get(LoginViewModel::class.java).also {
        ComponentInjector.component.inject(it)
    } }

    private var doorType: DoorType? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_door, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        val supportActionBar = (requireActivity() as MainActivity).supportActionBar
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        when(getSharedPreferenceCompany(requireContext())) {
            "SI2001" -> supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_si_32dp)
            else -> supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_swipe_32dp)
        }

        mainViewModel.openDoorError.observe(viewLifecycleOwner, Observer {
            loading.visibility = View.GONE
            Snackbar.make(open_in_door_button, "Qualcosa è andato storto :(", Snackbar.LENGTH_LONG).show()
        })

        mainViewModel.openDoorResponse.observe(viewLifecycleOwner, Observer {
            loading.visibility = View.GONE
            Snackbar.make(open_in_door_button, "Porta Aperta!", Snackbar.LENGTH_LONG).show()
        })

        open_in_door_button.setOnClickListener {
             if(isOnline(requireContext())) {
                 doorType = DoorType.IN
                 loading.visibility = View.VISIBLE
                 openDoor()
             }
            else Snackbar.make(open_in_door_button, "Abilitare connessione ad internet", Snackbar.LENGTH_LONG).show()
        }

        open_out_door_button.setOnClickListener {
            if(isOnline(requireContext())) {
                doorType = DoorType.OUT
                loading.visibility = View.VISIBLE
                openDoor()
            }
            else Snackbar.make(open_out_door_button, "Abilitare connessione ad internet", Snackbar.LENGTH_LONG).show()
        }

        openDoor()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.logout -> {
                loginViewModel.logout()
                deleteSharedPreferenceCompany(requireActivity())
                deleteSharedPreferenceToken(requireActivity())
                startActivity(Intent(requireActivity().applicationContext, LoginActivity::class.java))
                requireActivity().finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openDoor() {
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
                    &&
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                // You can use the API that requires the permission.
                getCurrentLocation()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) -> {
                requestPermissions.launch(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION))
            }
            else -> {
                // We can request the permission by launching the ActivityResultLauncher
                requestPermissions.launch(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION))
                // The registered ActivityResultCallback gets the result of the request.
            }

        }
    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {

        val locationManager = ContextCompat.getSystemService(requireContext(), LocationManager::class.java)

        if(locationManager != null && LocationManagerCompat.isLocationEnabled(locationManager)) {
            locationManager.requestSingleUpdate(Criteria().apply { accuracy = Criteria.ACCURACY_FINE } , this, null)
        } else {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }

    }

    private fun getDistanceFromLatLonInMeters(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val earthRadius = 6371
        val dLat = deg2rad(lat2 - lat1)
        val dLon = deg2rad(lon2 - lon1)
        val a = sin(dLat / 2) * sin(dLat / 2) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * sin(dLon / 2) *  sin(dLon / 2)
        val c = 2 *  atan2(sqrt(a),  sqrt(1 - a))
        return earthRadius * c * 1000
    }

    private fun deg2rad(deg: Double): Double{
        return deg * (Math.PI/180)
    }

    override fun onLocationChanged(location: Location?) {
        val lat = location?.latitude ?: return
        val long = location.longitude

        val trevioloLat = getString(R.string.treviolo_lat).toDouble()
        val trevioloLong = getString(R.string.treviolo_long).toDouble()

        val sestoLat = getString(R.string.sesto_lat).toDouble()
        val sestoLong = getString(R.string.sesto_long).toDouble()

        val trevioloDistance = getDistanceFromLatLonInMeters(lat, long, trevioloLat, trevioloLong)
        val sestoDistance = getDistanceFromLatLonInMeters(lat, long, sestoLat, sestoLong)

        when {
            trevioloDistance <= MIN_DISTANCE -> {
                doorType?.let { mainViewModel.openDoor( OpenDoorRequest("treviolo", "Bearer " + getSharedPreferenceToken(requireActivity() ), it) ) }
                //mainViewModel.getDoors(DoorRequest("treviolo", "Bearer " + getSharedPreferenceToken(requireActivity())))
            }
            sestoDistance <= MIN_DISTANCE -> {
                doorType?.let { mainViewModel.openDoor( OpenDoorRequest("sesto-san-giovanni", "Bearer " + getSharedPreferenceToken(requireActivity() ), it) ) }
                //mainViewModel.getDoors(DoorRequest("sesto-san-giovanni", "Bearer " + getSharedPreferenceToken(requireActivity())))
            }
            else-> {
                Snackbar.make(door_constraint_layout, R.string.no_near_office , Snackbar.LENGTH_SHORT).show()
                loading.visibility = View.GONE
            }

        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

    companion object {
        const val MIN_DISTANCE = 100
    }
}
