package com.si2001.officedoor.utils

import android.content.Context


fun setSharedPreferenceToken(context: Context, token: String ){
   context.getSharedPreferences("TokenPref",  Context.MODE_PRIVATE).also {
       it.edit().putString("token", token).commit()
   }
}

fun deleteSharedPreferenceToken(context: Context ){
    context.getSharedPreferences("TokenPref",  Context.MODE_PRIVATE).also {
        it.edit().remove("token").commit()
    }
}

fun getSharedPreferenceToken(context: Context ) : String? {
    context.getSharedPreferences("TokenPref",  Context.MODE_PRIVATE).also {
        return it.getString("token", "")
    }
}

fun setSharedPreferenceCompany(context: Context, company: String ){
    context.getSharedPreferences("TokenPref",  Context.MODE_PRIVATE).also {
        it.edit().putString("company", company).commit()
    }
}

fun deleteSharedPreferenceCompany(context: Context ){
    context.getSharedPreferences("TokenPref",  Context.MODE_PRIVATE).also {
        it.edit().remove("company").commit()
    }
}

fun getSharedPreferenceCompany(context: Context ) : String? {
    context.getSharedPreferences("TokenPref",  Context.MODE_PRIVATE).also {
        return it.getString("company", "")
    }
}
